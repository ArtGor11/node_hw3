const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}}),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    role: Joi.string().required(),
  });

  await schema.validateAsync(req.body);
  next();
};
