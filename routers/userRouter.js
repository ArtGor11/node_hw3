const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();
const { authMiddleWare } = require('./middlewares/authMiddleWare');
const { User } = require('../models/User');

router.get('/', authMiddleWare, async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    res.status(200).json({
      user: {
        id: user.id,
        email: user.email,
        created_date: user.created_date,
        role: user.role,
      },
    });
  } catch (error) {
    console.log('error: ', error);
    return res
      .status(500)
      .json({ message: 'Something went wrong, try again later' });
  }
});

router.delete('/', authMiddleWare, async (req, res) => {
  try {
    await User.findOneAndDelete({ email: req.user.email });
    res.status(200).json({ message: 'Profile deleted successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.patch('/', authMiddleWare, async (req, res) => {
  try {
    const user = await User.findOne({ email: req.user.email });
    if (!(await bcrypt.compare(req.body.oldPassword, user.password))) {
      return res.status(400).json({ message: 'Wrong password!' });
    }

    await User.updateOne(
      { email: req.user.email },
      { password: await bcrypt.hash(req.body.newPassword, 10) },
    );

    res.status(200).json({ message: 'Password changed successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

module.exports = router;
