const express = require('express');
const router = express.Router();
const { authMiddleWare } = require('../routers/middlewares/authMiddleWare');

const {
  getLoad,
  addUserLoad,
  getActiveLoads,
  iterateNextLoad,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadInfoById } = require('../controllers/loadController');

router.get('/', authMiddleWare, getLoad);
router.post('/', authMiddleWare, addUserLoad);
router.get('/active', authMiddleWare, getActiveLoads);
router.patch('/active/state', authMiddleWare, iterateNextLoad);
router.get('/:id', authMiddleWare, getLoadById);
router.put('/:id', authMiddleWare, updateLoadById);
router.delete('/:id', authMiddleWare, deleteLoadById);
router.post('/:id/post', authMiddleWare, postLoadById);
router.get('/:id/shipping_info', authMiddleWare, getLoadInfoById);

module.exports = router;
