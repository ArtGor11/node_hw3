const mongoose = require('mongoose');
const validator = require('validator');
const { Schema } = mongoose;

const userSchema = new Schema({
  email: {
    required: true,
    type: String,
    unique: true,
    validate: (value) => {
      return validator.isEmail(value);
    },
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
},
);

module.exports.User = mongoose.model('User', userSchema);
