const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const config = require('./config/default.json');
const {dbconfig} = config;
const {port} = config;
const {db_user, db_password, db_name} = dbconfig;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
  console.log(err.message);
});

const start = async () => {
  await mongoose.connect(`mongodb+srv://${db_user}:${db_password}@cluster0.0xvle.mongodb.net/${db_name}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server is running at port ${port}!`);
  });
};

start();
